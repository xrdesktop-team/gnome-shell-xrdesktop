From b8606645488957e5b2628c93d96bccb825a843b9 Mon Sep 17 00:00:00 2001
From: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
Date: Wed, 24 Jul 2019 16:13:46 +0200
Subject: [PATCH 08/30] vr-mirror: Improve parent detection and 3D window
 placement.

Make pixels per meter constant a define.
Extract function to apply desktop transformation to a window.
Treat modal dialog as child window.
Don't crash if parent is not available.
Use meta window transients and root windows, refactor parenting code.
Position new windows at desktop positon, on top layer.
---
 src/shell-vr-mirror.c | 171 ++++++++++++++++++++++++++++--------------
 1 file changed, 115 insertions(+), 56 deletions(-)

diff --git a/src/shell-vr-mirror.c b/src/shell-vr-mirror.c
index 4af2b5d15..fd7485f53 100644
--- a/src/shell-vr-mirror.c
+++ b/src/shell-vr-mirror.c
@@ -27,7 +27,7 @@
 static ShellVRMirror *shell_vr_mirror_instance = NULL;
 
 /* 1 pixel/meter = 0.0254 dpi */
-static const float pixels_per_meter = 720.0;
+#define SHELL_VR_PIXELS_PER_METER 720.0f
 #define SHELL_VR_DESKTOP_PLANE_DISTANCE 3.5f
 #define SHELL_VR_LAYER_DISTANCE 0.1f
 
@@ -163,6 +163,8 @@ struct _ShellVRMirror
   GulkanTexture *cursor_gulkan_texture;
   GLuint cursor_gl_texture;
 
+  uint32_t top_layer;
+
   gboolean nvidia;
 };
 
@@ -337,7 +339,9 @@ _meta_win_to_xrd_window (ShellVRMirror *self, MetaWindow *meta_window)
 
 static void
 shell_vr_mirror_init (ShellVRMirror *self)
-{}
+{
+  self->top_layer = 0;
+}
 
 ShellVRMirror *
 shell_vr_mirror_new (void)
@@ -738,7 +742,37 @@ _mirror_current_windows (ShellVRMirror *self)
     }
 }
 
-void
+static void
+_apply_desktop_position (MetaWindow *meta_win,
+                         XrdWindow  *xrd_win,
+                         uint32_t    layer)
+{
+  ShellGlobal *global = shell_global_get ();
+  MetaDisplay *display = shell_global_get_display (global);
+
+  int screen_w, screen_h;
+  meta_display_get_size (display, &screen_w, &screen_h);
+
+  MetaRectangle rect;
+  meta_window_get_buffer_rect (meta_win, &rect);
+
+  float x =            rect.x - screen_w / 2.0f + rect.width  / 2.0f;
+  float y = screen_h - rect.y - screen_h / 4.0f - rect.height / 2.0f;
+
+  graphene_point3d_t point = {
+    .x = x / SHELL_VR_PIXELS_PER_METER,
+    .y = y / SHELL_VR_PIXELS_PER_METER + DEFAULT_LEVEL,
+    .z = -SHELL_VR_DESKTOP_PLANE_DISTANCE + SHELL_VR_LAYER_DISTANCE * layer
+  };
+
+  graphene_matrix_t transform;
+  graphene_matrix_init_translate (&transform, &point);
+
+  xrd_window_set_transformation (xrd_win, &transform);
+  xrd_window_save_reset_transformation (xrd_win);
+}
+
+static void
 _arrange_windows_by_desktop_position (ShellVRMirror *self)
 {
   GSList *xrd_win_list = xrd_client_get_windows (self->client);
@@ -766,39 +800,17 @@ _arrange_windows_by_desktop_position (ShellVRMirror *self)
   GSList *sorted_windows =
     meta_display_sort_windows_by_stacking (display, meta_win_list);
 
-  for (uint32_t i = 0; sorted_windows; sorted_windows = sorted_windows->next)
+  for (self->top_layer = 0; sorted_windows;
+       sorted_windows = sorted_windows->next)
     {
       XrdWindow *xrd_window = _meta_win_to_xrd_window (self,
                                                        sorted_windows->data);
       if (!xrd_window)
-        {
-          g_print ("Skipping window...\n");
-          continue;
-        }
-
-      int screen_w, screen_h;
-      meta_display_get_size (display, &screen_w, &screen_h);
-
-      MetaRectangle rect;
-      meta_window_get_buffer_rect (sorted_windows->data, &rect);
-      g_print ("Resizing MetaWindowActor [%d,%d] %dx%d\n",
-               rect.x, rect.y, rect.width, rect.height);
-
-      float x =            rect.x - screen_w / 2.0f + rect.width  / 2.0f;
-      float y = screen_h - rect.y - screen_h / 4.0f - rect.height / 2.0f;
-      graphene_point3d_t point = {
-        .x = x / pixels_per_meter,
-        .y = y / pixels_per_meter + DEFAULT_LEVEL,
-        .z = -SHELL_VR_DESKTOP_PLANE_DISTANCE + SHELL_VR_LAYER_DISTANCE * i
-      };
-
-      graphene_matrix_t transform;
-      graphene_matrix_init_translate (&transform, &point);
-
-      xrd_window_set_transformation (xrd_window, &transform);
-      xrd_window_save_reset_transformation (xrd_window);
+        continue;
 
-      i++;
+      _apply_desktop_position (sorted_windows->data,
+                               xrd_window, self->top_layer);
+      self->top_layer++;
     }
   g_slist_free (sorted_windows);
 }
@@ -898,9 +910,54 @@ _is_child_window (MetaWindow *meta_win)
     t == META_WINDOW_POPUP_MENU ||
     t == META_WINDOW_DROPDOWN_MENU ||
     t == META_WINDOW_TOOLTIP ||
+    t == META_WINDOW_MODAL_DIALOG ||
     t == META_WINDOW_COMBO;
 }
 
+static XrdWindow*
+_get_valid_xrd_parent (ShellVRMirror *self,
+                       MetaWindow    *meta_parent)
+{
+  XrdWindow *xrd_parent = NULL;
+  if (meta_parent != NULL && !_is_excluded_from_mirroring (meta_parent))
+    xrd_parent = xrd_client_lookup_window (self->client, meta_parent);
+  return xrd_parent;
+}
+
+static bool
+_find_valid_parent (ShellVRMirror *self,
+                    MetaWindow    *child,
+                    MetaWindow   **meta_parent,
+                    XrdWindow    **xrd_parent)
+{
+  /* Try transient first */
+  *meta_parent = meta_window_get_transient_for (child);
+  *xrd_parent = _get_valid_xrd_parent (self, *meta_parent);
+  if (*xrd_parent != NULL)
+    return TRUE;
+
+  /* If this doesn't work out try the root ancestor */
+  *meta_parent = meta_window_find_root_ancestor (child);
+  *xrd_parent = _get_valid_xrd_parent (self, *meta_parent);
+  if (*xrd_parent != NULL)
+    return TRUE;
+
+  /* Last try, check if anything is focused and make that our parent */
+  ShellGlobal *global = shell_global_get ();
+  MetaDisplay *display = shell_global_get_display (global);
+  *meta_parent = meta_display_get_focus_window (display);
+  *xrd_parent = _get_valid_xrd_parent (self, *meta_parent);
+  if (*xrd_parent != NULL)
+    return TRUE;
+
+  /* Didn't find anything */
+  const gchar *title = meta_window_get_title (child);
+  g_warning ("Could not find a parent for `%s`", title);
+
+  return FALSE;
+
+}
+
 gboolean
 shell_vr_mirror_map_actor (ShellVRMirror   *self,
                            MetaWindowActor *actor)
@@ -916,33 +973,30 @@ shell_vr_mirror_map_actor (ShellVRMirror   *self,
   meta_window_get_buffer_rect (meta_win, &rect);
 
   gboolean is_child = _is_child_window (meta_win);
-  XrdWindow *parent = NULL;
-  MetaWindow *parent_meta_win = NULL;
+  XrdWindow *xrd_parent = NULL;
+  MetaWindow *meta_parent = NULL;
+
   if (is_child)
     {
-      ShellGlobal *global = shell_global_get ();
-      MetaDisplay *display = shell_global_get_display (global);
-      parent_meta_win = meta_display_get_focus_window (display);
-
-      if (parent_meta_win != NULL &&
-          !_is_excluded_from_mirroring (parent_meta_win))
+      if (_find_valid_parent (self, meta_win, &meta_parent, &xrd_parent))
         {
-          parent = xrd_client_lookup_window (self->client, parent_meta_win);
-
-          /* each window only has one child window, chain this child window to the
-             last existing child window of the parent window.
-             TODO: does this cover all wayland cases? */
-          XrdWindowData *parent_data = xrd_window_get_data (parent);
+          /*
+           * Each window only has one child window,
+           * chain this child window to the last
+           * existing child window of the parent window.
+           * TODO: Test this on Wayland
+           */
+          XrdWindowData *parent_data = xrd_window_get_data (xrd_parent);
           while (parent_data->child_window != NULL)
             {
-              parent = parent_data->child_window->xrd_window;
-              parent_data = xrd_window_get_data (parent);
+              xrd_parent = parent_data->child_window->xrd_window;
+              parent_data = xrd_window_get_data (xrd_parent);
               ShellVRWindow *parent_shell_win;
-              g_object_get (parent, "native", &parent_shell_win, NULL);
+              g_object_get (xrd_parent, "native", &parent_shell_win, NULL);
               if (parent_shell_win)
                 {
                   MetaWindowActor *actor = parent_shell_win->meta_window_actor;
-                  parent_meta_win = meta_window_actor_get_meta_window (actor);
+                  meta_parent = meta_window_actor_get_meta_window (actor);
                 }
             }
         }
@@ -954,21 +1008,26 @@ shell_vr_mirror_map_actor (ShellVRMirror   *self,
 
   XrdWindow *xrd_win =
     xrd_client_window_new_from_pixels (self->client, title, rect.width,
-                                       rect.height, pixels_per_meter);
+                                       rect.height, SHELL_VR_PIXELS_PER_METER);
 
-  gboolean draggable = !(is_child && parent_meta_win != NULL && parent != NULL);
+  gboolean draggable = !(is_child && meta_parent != NULL && xrd_parent != NULL);
   xrd_client_add_window (self->client, xrd_win, draggable, meta_win);
 
   if (is_child && !draggable)
     {
       graphene_point_t offset;
-      _get_offset (parent_meta_win, meta_win, &offset);
+      _get_offset (meta_parent, meta_win, &offset);
 
-      xrd_window_add_child (parent, xrd_win, &offset);
+      xrd_window_add_child (xrd_parent, xrd_win, &offset);
+    }
+  else if (is_child && xrd_parent == NULL)
+    g_warning ("Can't add window '%s' as child. No parent candidate!\n", title);
+
+  if (!is_child)
+    {
+      _apply_desktop_position (meta_win, xrd_win, self->top_layer);
+      self->top_layer++;
     }
-  else if (is_child && parent == NULL)
-      g_print ("WARN: Can't add window '%s' as child. No parent candidate!\n",
-               title);
 
   ShellVRWindow *shell_win = g_malloc (sizeof (ShellVRWindow));
   shell_win->meta_window_actor = actor;
-- 
2.20.1

